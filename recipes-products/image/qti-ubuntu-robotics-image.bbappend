# -----------------------------------------------------------------------------------------------------------------------------
#  This bbappend appends to the image recipe allowing us to more freely modify the system image
#  This allows for things such as CORE_IMAGE_BASE_INSTALL and ROOTFS_POSTPROCESS_COMMAND
# -----------------------------------------------------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------------------------------------------------
# Local recipe packages added or removed from the system image
# -----------------------------------------------------------------------------------------------------------------------------

# General platform independent recipes
CORE_IMAGE_BASE_INSTALL += " \
    mv \
    rtl8188eus \
    rtl8812au \
    rtl8812au-vtx \
    rtl8821cu \
    qrb5165-slpi-test-sig \
    qrb5165-bind \
    cci-direct \
    en-slpi-restart \
"

# M0054 specific recipes
CORE_IMAGE_BASE_INSTALL += " \
    ${@bb.utils.contains('PLATFORM', 'M0054', 'voxl2-system-image', '', d)} \
    ${@bb.utils.contains('PLATFORM', 'M0054', 'voxl2-wlan', '', d)} \
"

# M0104 specific recipes
CORE_IMAGE_BASE_INSTALL += " \
    ${@bb.utils.contains('PLATFORM', 'M0104', 'voxl2-system-image', '', d)} \
    ${@bb.utils.contains('PLATFORM', 'M0104', 'voxl2-wlan', '', d)} \
"

# M0052 specific recipes
CORE_IMAGE_BASE_INSTALL += " \
    ${@bb.utils.contains('PLATFORM', 'M0052', 'rb5-system-image', '', d)} \
"

# Recipe packages removed from system image
# tdk-chx01-get-data-app      : unnecessary tdk chirp binaries
# tdk-hvc4223f-scripts        : unnecessary tdk python scripts
# tdk-thermistor-app          : unnecessary tdk thermistor binaries

CORE_IMAGE_BASE_INSTALL_remove = " \
    tdk-chx01-get-data-app \
    tdk-hvc4223f-scripts \
    tdk-thermistor-app \
"


# -----------------------------------------------------------------------------------------------------------------------------
# Rootfs postprocess commands that run after rootfs has been generated 
# -----------------------------------------------------------------------------------------------------------------------------

ROOTFS_POSTPROCESS_COMMAND += " do_fix_wayland_symlink; "
ROOTFS_POSTPROCESS_COMMAND += " do_enable_root_login; "
ROOTFS_POSTPROCESS_COMMAND += " do_configure_ssh_env; "
ROOTFS_POSTPROCESS_COMMAND += " do_echo_platform_name; "
ROOTFS_POSTPROCESS_COMMAND += " do_add_ldconfig; "
ROOTFS_POSTPROCESS_COMMAND += " do_add_packages_list; "
ROOTFS_POSTPROCESS_COMMAND += " do_blacklist_vtx_driver; "
ROOTFS_POSTPROCESS_COMMAND += " do_journald_set_max_use; "

ROOTFS_POSTINSTALL_COMMAND += " do_factory_bundle_patch; "

# wayland symlinks are broken causing ldconfig to cry, fix issue by giving
# .so files appropriate sym links
do_fix_wayland_symlink() {
    rm ${IMAGE_ROOTFS}/usr/lib/libwayland-cursor.so.0
    rm ${IMAGE_ROOTFS}/usr/lib/libwayland-server.so.0
    rm ${IMAGE_ROOTFS}/usr/lib/libwayland-client.so.0

    ln -s ${IMAGE_ROOTFS}/usr/lib/libwayland-cursor.so.0.0.0 ${IMAGE_ROOTFS}/usr/lib/libwayland-cursor.so.0
    ln -s ${IMAGE_ROOTFS}/usr/lib/libwayland-server.so.0.1.0 ${IMAGE_ROOTFS}/usr/lib/libwayland-server.so.0
    ln -s ${IMAGE_ROOTFS}/usr/lib/libwayland-client.so.0.3.0 ${IMAGE_ROOTFS}/usr/lib/libwayland-client.so.0
}

# Grants permission for root user to ssh on target
do_enable_root_login() {
    sed -i 's/^[#[:space:]]*PermitRootLogin.*/PermitRootLogin yes/' ${IMAGE_ROOTFS}/etc/ssh/sshd_config
}

# Add bash profile script that will exec root bashrc for ssh users
do_configure_ssh_env() {
    echo -e "if [ -f "/home/root/.bashrc" ]; then\n\t. "/home/root/.bashrc"\nfi" > ${IMAGE_ROOTFS}/home/root/.bash_profile
}

# PS1 pulls platform name from this file, user can configure as they like
do_echo_platform_name() {
    if [ "${@bb.utils.contains('PLATFORM', 'M0104', 'true', 'false', d)}" = "true" ]; then
        echo -n voxl2-mini > ${IMAGE_ROOTFS}/usr/share/platform_name
    elif [ "${@bb.utils.contains('PLATFORM', 'M0054', 'true', 'false', d)}" = "true" ]; then
        echo -n voxl2 > ${IMAGE_ROOTFS}/usr/share/platform_name
    elif [ "${@bb.utils.contains('PLATFORM', 'M0052', 'true', 'false', d)}" = "true" ]; then
        echo -n rb5 > ${IMAGE_ROOTFS}/usr/share/platform_name
    fi
}

# add our own library locations to ld's search path
do_add_ldconfig() {
	echo "/usr/lib64" > ${IMAGE_ROOTFS}/etc/ld.so.conf.d/modalai.conf
	echo "/usr/lib32" >> ${IMAGE_ROOTFS}/etc/ld.so.conf.d/modalai.conf
}

# Add package list for pulling modalai repos
do_add_packages_list() {
    SOURCES_LIST="deb [trusted=yes] http://voxl-packages.modalai.com/ qrb5165 stable"
    echo $SOURCES_LIST > ${IMAGE_ROOTFS}/etc/apt/sources.list.d/modalai.list
}

# This is a hacky way to get libsns in the system image since the px4-support version gets overriden
do_factory_bundle_patch() {
    ${STAGING_BINDIR_NATIVE}/dpkg-deb -X ${DEPLOY_DIR_DEB}/aarch64/px4-support_1.0-6_arm64.deb ${EXTERNAL_TOOLCHAIN} # extract files to install section
    rm ${IMAGE_ROOTFS}/usr/lib/libsnsapi.so.1.0.0
    cp ${EXTERNAL_TOOLCHAIN}/usr/lib/libsnsapi.so.1.0.0 ${IMAGE_ROOTFS}/usr/lib
}

# Blacklist driver from loading by default
do_blacklist_vtx_driver() {
    echo blacklist 88XXau_vtx >> ${IMAGE_ROOTFS}/etc/modprobe.d/blacklist-88XXau_vtx.conf
}

do_journald_set_max_use() {
    sed -i 's/^[#]SystemMaxUse=.*/SystemMaxUse=1000M/' ${IMAGE_ROOTFS}/etc/systemd/journald.conf
}

# -----------------------------------------------------------------------------------------------------------------------------
# Custom partition generation
# -----------------------------------------------------------------------------------------------------------------------------

# Overriding system sizes for EXT4 (in bytes)
SYSTEM_SIZE_EXT4 = "51405389824"
USERDATA_SIZE_EXT4 = "68719476736"
MODALAI_CAL_SIZE_EXT4 = "67108864"
MODALAI_CONF_SIZE_EXT4 = "67108864"

OVERLAYIMAGE_TARGET = "${MACHINE}-data-fs.ext4"
MODALAI_CAL_TARGET = "${MACHINE}-cal-fs.ext4"
MODALAI_CONF_TARGET = "${MACHINE}-conf-fs.ext4"

do_makeuserdata[dirs] = "${IMGDEPLOYDIR}/${IMAGE_BASENAME}"
do_makemodalaical[dirs] = "${IMGDEPLOYDIR}/${IMAGE_BASENAME}"
do_makemodalaiconf[dirs] = "${IMGDEPLOYDIR}/${IMAGE_BASENAME}"

# Generate data filesystem for data partition
# This partition will be a users working data 
do_makeuserdata() {
    make_ext4fs -s -l ${USERDATA_SIZE_EXT4} ${IMAGE_EXT4_SELINUX_OPTIONS} \
        -a /data -b 4096 ${DEPLOY_DIR_IMAGE}/${OVERLAYIMAGE_TARGET} ${IMAGE_ROOTFS}/data
}
addtask do_makeuserdata after do_rootfs before do_build

# Generate calibration filesystem for calibration partition
# This partition will contain all modalai calibration files

do_makemodalaical() {
    mkdir -p ${IMAGE_ROOTFS}/data/modalai
    make_ext4fs -s -l ${MODALAI_CAL_SIZE_EXT4} ${IMAGE_EXT4_SELINUX_OPTIONS} \
        -a /modalai -b 4096 ${DEPLOY_DIR_IMAGE}/${MODALAI_CAL_TARGET} ${IMAGE_ROOTFS}/data/modalai
    
    chmod 644 ${DEPLOY_DIR_IMAGE}/${MODALAI_CAL_TARGET}
}
addtask do_makemodalaical after do_rootfs before do_build

# Generate configuration filesystem for configuration partition 
# This partition will contain all modalai configuration files
do_makemodalaiconf() {
    mkdir -p ${IMAGE_ROOTFS}/etc/modalai
    make_ext4fs -s -l ${MODALAI_CONF_SIZE_EXT4} ${IMAGE_EXT4_SELINUX_OPTIONS} \
        -a /modalai -b 4096 ${DEPLOY_DIR_IMAGE}/${MODALAI_CONF_TARGET} ${IMAGE_ROOTFS}/etc/modalai
    
    chmod 644 ${DEPLOY_DIR_IMAGE}/${MODALAI_CONF_TARGET}
}
addtask do_makemodalaiconf after do_rootfs before do_build
