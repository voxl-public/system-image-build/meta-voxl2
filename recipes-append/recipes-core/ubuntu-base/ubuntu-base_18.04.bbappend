# -----------------------------------------------------------------------------------------------------------------------------
#  This bbappend appends to the ubuntu base which installs and configures ubuntu
#  We can directly make modifications here to help us configure our ubuntu environment
# -----------------------------------------------------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------------------------------------------------
# apt packages to include in the system image
# -----------------------------------------------------------------------------------------------------------------------------

UBUN_FULLSTACK_DEBUG_PACKAGES += "git cmake screen device-tree-compiler udhcpc libqmi-utils sudo openvpn iptables libgstreamer1.0-0 gstreamer1.0-plugins-base xterm wpasupplicant stress valgrind jq pciutils aircrack-ng i2c-tools python3-pip python3-serial python3-numpy nano libexif12 libboost-filesystem-dev libboost-thread-dev libboost-date-time-dev neofetch libsdl2-dev iperf3 htop tmux screen tcpdump rsync python3-progressbar"
UBUN_FULLSTACK_PERF_PACKAGES += "git cmake udhcpc libqmi-utils sudo openvpn iptables libgstreamer1.0-0 gstreamer1.0-plugins-base xterm wpasupplicant stress valgrind jq pciutils aircrack-ng i2c-tools python3-pip python3-serial python3-numpy nano libexif12 libboost-filesystem-dev libboost-thread-dev libboost-date-time-dev neofetch libsdl2-dev iperf3 htop tmux screen tcpdump rsync python3-progressbar"

# The ubuntu installation brings many unwanted packages to the system image.
# This variable contains all the packages that we want apt to remove from the ubuntu installation to avoid conflict with our custom software packages
# or to reduce the size of the system image
UBUN_DEFAULT_PACKAGES_TO_REMOVE += "libopencv-calib3d-dev libopencv-calib3d3.2 libopencv-contrib-dev libopencv-contrib3.2 \
                                    libopencv-core-dev libopencv-core3.2 libopencv-dev libopencv-features2d-dev libopencv-features2d3.2 libopencv-flann-dev libopencv-flann3.2 \
                                    libopencv-highgui-dev libopencv-highgui3.2 libopencv-imgcodecs-dev libopencv-imgcodecs3.2 libopencv-imgproc-dev libopencv-imgproc3.2 \
                                    libopencv-ml-dev libopencv-ml3.2 libopencv-objdetect-dev libopencv-objdetect3.2 libopencv-photo-dev libopencv-photo3.2 \
                                    libopencv-shape-dev libopencv-shape3.2 libopencv-stitching-dev libopencv-stitching3.2 libopencv-superres-dev libopencv-superres3.2 \
                                    libopencv-ts-dev libopencv-video-dev libopencv-video3.2 libopencv-videoio-dev libopencv-videoio3.2 libopencv-videostab-dev \
                                    libopencv-videostab3.2 libopencv-viz-dev libopencv-viz3.2 libopencv3.2-java libopencv3.2-jni opencv-data \
"


# -----------------------------------------------------------------------------------------------------------------------------
# ubuntu install modifications
# -----------------------------------------------------------------------------------------------------------------------------

do_ubuntu_install_append() {

    # Remove unwanted packages and clear cache
    fakechroot fakeroot  chroot ${TMP_WKDIR} /bin/bash -c "export DEBIAN_FRONTEND=noninteractive; apt-get remove ${UBUN_DEFAULT_PACKAGES_TO_REMOVE} -y"
    fakechroot fakeroot  chroot ${TMP_WKDIR} /bin/bash -c "export DEBIAN_FRONTEND=noninteractive; apt-get autoremove -y"

    # Change root home from / to /home/root
    mkdir -p ${TMP_WKDIR}/home/root
    chmod 700 ${TMP_WKDIR}/home/root
    sed -i 's_root:x:0:0:root:/root:/bin/bash_root:x:0:0:root:/home/root:/bin/bash_g' ${TMP_WKDIR}/etc/passwd

    # clear cached packages, should save around 400MB of space
    fakechroot fakeroot  chroot ${TMP_WKDIR} /bin/bash -c "export DEBIAN_FRONTEND=noninteractive; apt -y clean"

}
