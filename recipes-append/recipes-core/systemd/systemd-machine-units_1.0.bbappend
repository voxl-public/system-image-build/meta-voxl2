FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append += " file://etc-modalai.mount"
SRC_URI_append += " file://data-modalai.mount"

do_install_append () {
    install -d 0644 ${D}${sysconfdir}/initscripts
    install -d 0644 ${D}${systemd_unitdir}/system
    install -d 0644 ${D}${systemd_unitdir}/system/local-fs.target.requires
    install -d 0644 ${D}${systemd_unitdir}/system/local-fs.target.wants
    install -d 0644 ${D}${systemd_unitdir}/system/sysinit.target.wants
    install -d 0644 ${D}${systemd_unitdir}/system/multi-user.target.wants

    if ${@bb.utils.contains('DISTRO_FEATURES','nand-boot','false','true',d)}; then
        # install modalai_conf mount
        install -m 0644 ${WORKDIR}/etc-modalai.mount ${D}${systemd_unitdir}/system/etc-modalai.mount
        ln -sf ${systemd_unitdir}/system/etc-modalai.mount ${D}${systemd_unitdir}/system/multi-user.target.wants/etc-modalai.mount

        # install modalai_cal mount
        install -m 0644 ${WORKDIR}/data-modalai.mount ${D}${systemd_unitdir}/system/data-modalai.mount
        ln -sf ${systemd_unitdir}/system/data-modalai.mount ${D}${systemd_unitdir}/system/multi-user.target.wants/data-modalai.mount
    fi
}
