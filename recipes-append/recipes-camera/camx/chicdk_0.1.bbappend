CHI_PREBUILT_PATH = "${WORKDIR}/chi-cdk/vendor/prebuilt/"

MACHINE_TYPE ?= "${MACHINE}"

do_install_append() {
	# occasionally, we ship prebuil bins
	if [ -d "${CHI_PREBUILT_PATH}" ]; then
		cp -rf ${CHI_PREBUILT_PATH}/* ${D}${libdir}/camera/
	fi

	bbnote "ModalAI: chi-cdk update for: ${MACHINE_TYPE}"

	# see lu.um.1.2.1/apps_proc/src/vendor/qcom/proprietary/chi-cdk/tools/buildbins/buildbins_kona.yaml

	###############################################################################

	#
	# ar0144 : sensors 0-5 (8 bit)
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_1.bin ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_3.bin ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_4.bin ${D}/usr/share/modalai/chi-cdk/ar0144
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_5.bin ${D}/usr/share/modalai/chi-cdk/ar0144

	#
	# ar0144 : sensors 0-5 (10 bit)
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_1.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_3.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_4.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_5.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit
	
	#
	# ar0144 : sensors 0-5 (12 bit)
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_1.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_3.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_4.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_5.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit
	
	#
	# ar0144-combo : sensors 0, 6
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_combo_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_combo_6.bin ${D}/usr/share/modalai/chi-cdk/ar0144-combo

	#
	# ar0144-10bit-combo : sensors 0, 6
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-10bit-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_combo_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_combo_6.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit-combo

	#
	# ar0144-12bit-combo : sensors 0, 6
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-12bit-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_combo_0.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_combo_6.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit-combo


	#
	# ar0144-fsin : sensor 2
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_fsin_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144-fsin

	#
	# ar0144-10bit-fsin : sensor 2
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-10bit-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_10bit_fsin_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144-10bit-fsin

	#
	# ar0144-12bit-fsin : sensor 2
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ar0144-12bit-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ar0144_12bit_fsin_2.bin ${D}/usr/share/modalai/chi-cdk/ar0144-12bit-fsin

	#
	# ov7251 : sensors 0-5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_0.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_1.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_2.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_3.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_4.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_5.bin ${D}/usr/share/modalai/chi-cdk/ov7251
	#
	# ov7251-fsin : sensors 0-5, fysnc input
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_0.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_1.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_2.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_3.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_4.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsin_5.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsin
	#
	# ov7251-fsout : sensors 0-5, fysnc output
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_0.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_1.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_2.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_3.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_4.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_fsout_5.bin ${D}/usr/share/modalai/chi-cdk/ov7251-fsout
	#
	# ov7251-combo : sensors 0/1 and 4/5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov7251-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_0.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_1.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_4.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_5.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo
	#
	# ov7251-combo-flip : sensors 0/1 and 4/5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov7251-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_flip_0.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_flip_1.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_flip_4.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov7251_combo_flip_5.bin ${D}/usr/share/modalai/chi-cdk/ov7251-combo-flip

	###############################################################################

	#
	# ov64b40
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_0.bin ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_1.bin ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_2.bin ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_3.bin ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_4.bin ${D}/usr/share/modalai/chi-cdk/ov64b40
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov64b40_5.bin ${D}/usr/share/modalai/chi-cdk/ov64b40

	###############################################################################

	#
	# ov9782 : sensor 2
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov9782
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_0.bin ${D}/usr/share/modalai/chi-cdk/ov9782
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_2.bin ${D}/usr/share/modalai/chi-cdk/ov9782
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_3.bin ${D}/usr/share/modalai/chi-cdk/ov9782
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_4.bin ${D}/usr/share/modalai/chi-cdk/ov9782

	#
	# ov9782-combo : sensors 0/1 4/5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov9782-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_0.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_1.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_4.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_5.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo

	#
	# ov9782-combo-flip : sensors 0/1 4/5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/ov9782-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_flip_0.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_flip_1.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_flip_4.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.ov9782_combo_flip_5.bin ${D}/usr/share/modalai/chi-cdk/ov9782-combo-flip

	###############################################################################


	#
	# irs1645 : sensors 0-5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_0.bin ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_1.bin ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_2.bin ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_3.bin ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_4.bin ${D}/usr/share/modalai/chi-cdk/irs1645
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs1645_5.bin ${D}/usr/share/modalai/chi-cdk/irs1645

	###############################################################################


	#
	# irs2975c : sensors 0-5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_0.bin ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_1.bin ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_2.bin ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_3.bin ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_4.bin ${D}/usr/share/modalai/chi-cdk/irs2975c
	mv ${D}${libdir}/camera/com.qti.sensormodule.irs2975c_5.bin ${D}/usr/share/modalai/chi-cdk/irs2975c

	###############################################################################


	#
	# imx214 : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx214
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_0.bin ${D}/usr/share/modalai/chi-cdk/imx214
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_2.bin ${D}/usr/share/modalai/chi-cdk/imx214
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_3.bin ${D}/usr/share/modalai/chi-cdk/imx214
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_4.bin ${D}/usr/share/modalai/chi-cdk/imx214

	###############################################################################


	#
	# imx214-flip : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx214-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_flip_0.bin ${D}/usr/share/modalai/chi-cdk/imx214-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_flip_2.bin ${D}/usr/share/modalai/chi-cdk/imx214-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_flip_3.bin ${D}/usr/share/modalai/chi-cdk/imx214-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx214_flip_4.bin ${D}/usr/share/modalai/chi-cdk/imx214-flip

	###############################################################################


	#
	# imx335 : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx335
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx335_0.bin ${D}/usr/share/modalai/chi-cdk/imx335
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx335_2.bin ${D}/usr/share/modalai/chi-cdk/imx335
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx335_3.bin ${D}/usr/share/modalai/chi-cdk/imx335
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx335_4.bin ${D}/usr/share/modalai/chi-cdk/imx335

	###############################################################################


	#
	# imx377 : sensors 3
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx377
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx377_3.bin ${D}/usr/share/modalai/chi-cdk/imx377

	###############################################################################


	#
	# imx412 : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx412
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_0.bin ${D}/usr/share/modalai/chi-cdk/imx412
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_2.bin ${D}/usr/share/modalai/chi-cdk/imx412
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_3.bin ${D}/usr/share/modalai/chi-cdk/imx412
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_4.bin ${D}/usr/share/modalai/chi-cdk/imx412

	###############################################################################

	#
	# imx412_flip : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx412-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_flip_0.bin ${D}/usr/share/modalai/chi-cdk/imx412-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_flip_2.bin ${D}/usr/share/modalai/chi-cdk/imx412-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_flip_3.bin ${D}/usr/share/modalai/chi-cdk/imx412-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_flip_4.bin ${D}/usr/share/modalai/chi-cdk/imx412-flip

	###############################################################################

	#
	# imx412_fpv : sensors 0, 2, 3 and 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx412-fpv
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_fpv_0.bin ${D}/usr/share/modalai/chi-cdk/imx412-fpv
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_fpv_1.bin ${D}/usr/share/modalai/chi-cdk/imx412-fpv
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_fpv_2.bin ${D}/usr/share/modalai/chi-cdk/imx412-fpv
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_fpv_3.bin ${D}/usr/share/modalai/chi-cdk/imx412-fpv
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx412_fpv_4.bin ${D}/usr/share/modalai/chi-cdk/imx412-fpv

	###############################################################################

	#
	# imx664
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_0.bin ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_1.bin ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_2.bin ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_3.bin ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_4.bin ${D}/usr/share/modalai/chi-cdk/imx664
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx664_5.bin ${D}/usr/share/modalai/chi-cdk/imx664

	#
	# imx678 : sensors 0, 2, 3, 4 and 5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_0.bin ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_mod_1.bin ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_2.bin ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_3.bin ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_4.bin ${D}/usr/share/modalai/chi-cdk/imx678
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_mod_5.bin ${D}/usr/share/modalai/chi-cdk/imx678

	###############################################################################


	#
	# imx678_flip : sensors 0, 2, 3, 4
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/imx678-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_flip_0.bin ${D}/usr/share/modalai/chi-cdk/imx678-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_flip_2.bin ${D}/usr/share/modalai/chi-cdk/imx678-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_flip_3.bin ${D}/usr/share/modalai/chi-cdk/imx678-flip
	mv ${D}${libdir}/camera/com.qti.sensormodule.imx678_flip_4.bin ${D}/usr/share/modalai/chi-cdk/imx678-flip

	###############################################################################


	#
	# boson : sensor 0-5
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_0.bin ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_1.bin ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_2.bin ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_3.bin ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_4.bin ${D}/usr/share/modalai/chi-cdk/boson
	mv ${D}${libdir}/camera/com.qti.sensormodule.boson_5.bin ${D}/usr/share/modalai/chi-cdk/boson


	###############################################################################
	###############################################################################


	#
	# tuning
	#
	mkdir -p ${D}/usr/share/modalai/chi-cdk/tuned
	cp ${D}${libdir}/camera/com.qti.tuned.default.bin ${D}/usr/share/modalai/chi-cdk/tuned
	mv ${D}${libdir}/camera/com.qti.tuned.imx214_m0024.bin ${D}/usr/share/modalai/chi-cdk/tuned

	###############################################################################
	###############################################################################

	#
	# by now we've moved everything out of usr/lib/camera/, but just in case,
	# clear this directory out of sensormodule bins for our scheme with 
	# voxl-camera-server to work
	#
	rm -f ${D}${libdir}/camera/com.qti.sensormodule.*
}

# ar0144 (8 bit)
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_0.bin \
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_1.bin \
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_2.bin \
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_3.bin \
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_4.bin \
	/usr/share/modalai/chi-cdk/ar0144/com.qti.sensormodule.ar0144_5.bin \
	"
# ar0144_10bit
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_0.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_1.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_2.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_3.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_4.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit/com.qti.sensormodule.ar0144_10bit_5.bin \
	"

# ar0144_12bit
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_0.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_1.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_2.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_3.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_4.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit/com.qti.sensormodule.ar0144_12bit_5.bin \
	"

# ar0144_combo
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-combo/com.qti.sensormodule.ar0144_combo_0.bin \
	/usr/share/modalai/chi-cdk/ar0144-combo/com.qti.sensormodule.ar0144_combo_6.bin \
	"

# ar0144_10bit_combo
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-10bit-combo/com.qti.sensormodule.ar0144_10bit_combo_0.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit-combo/com.qti.sensormodule.ar0144_10bit_combo_6.bin \
	"

# ar0144_12bit_combo
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-12bit-combo/com.qti.sensormodule.ar0144_12bit_combo_0.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit-combo/com.qti.sensormodule.ar0144_12bit_combo_6.bin \
	"

# ar0144_fsin
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ar0144-fsin/com.qti.sensormodule.ar0144_fsin_2.bin \
	/usr/share/modalai/chi-cdk/ar0144-10bit-fsin/com.qti.sensormodule.ar0144_10bit_fsin_2.bin \
	/usr/share/modalai/chi-cdk/ar0144-12bit-fsin/com.qti.sensormodule.ar0144_12bit_fsin_2.bin \
	"

# ov7251
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_0.bin \
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_1.bin \
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_2.bin \
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_3.bin \
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_4.bin \
	/usr/share/modalai/chi-cdk/ov7251/com.qti.sensormodule.ov7251_5.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_0.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_1.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_2.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_3.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_4.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsin/com.qti.sensormodule.ov7251_fsin_5.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_0.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_1.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_2.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_3.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_4.bin \
	/usr/share/modalai/chi-cdk/ov7251-fsout/com.qti.sensormodule.ov7251_fsout_5.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_0.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_1.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_4.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo/com.qti.sensormodule.ov7251_combo_5.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo-flip/com.qti.sensormodule.ov7251_combo_flip_0.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo-flip/com.qti.sensormodule.ov7251_combo_flip_1.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo-flip/com.qti.sensormodule.ov7251_combo_flip_4.bin \
	/usr/share/modalai/chi-cdk/ov7251-combo-flip/com.qti.sensormodule.ov7251_combo_flip_5.bin \
	"

# ov64b40
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_0.bin \
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_1.bin \
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_2.bin \
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_3.bin \
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_4.bin \
	/usr/share/modalai/chi-cdk/ov64b40/com.qti.sensormodule.ov64b40_5.bin \
	"

# ov9782
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/ov9782/com.qti.sensormodule.ov9782_0.bin \
	/usr/share/modalai/chi-cdk/ov9782/com.qti.sensormodule.ov9782_2.bin \
	/usr/share/modalai/chi-cdk/ov9782/com.qti.sensormodule.ov9782_3.bin \
	/usr/share/modalai/chi-cdk/ov9782/com.qti.sensormodule.ov9782_4.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_0.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_1.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_4.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo/com.qti.sensormodule.ov9782_combo_5.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo-flip/com.qti.sensormodule.ov9782_combo_flip_0.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo-flip/com.qti.sensormodule.ov9782_combo_flip_1.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo-flip/com.qti.sensormodule.ov9782_combo_flip_4.bin \
	/usr/share/modalai/chi-cdk/ov9782-combo-flip/com.qti.sensormodule.ov9782_combo_flip_5.bin \
	"

# irs1645
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_0.bin \
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_1.bin \
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_2.bin \
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_3.bin \
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_4.bin \
	/usr/share/modalai/chi-cdk/irs1645/com.qti.sensormodule.irs1645_5.bin \
	"

# irs2975c
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_0.bin \
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_1.bin \
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_2.bin \
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_3.bin \
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_4.bin \
	/usr/share/modalai/chi-cdk/irs2975c/com.qti.sensormodule.irs2975c_5.bin \
	"

# imx214
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx214/com.qti.sensormodule.imx214_0.bin \
	/usr/share/modalai/chi-cdk/imx214/com.qti.sensormodule.imx214_2.bin \
	/usr/share/modalai/chi-cdk/imx214/com.qti.sensormodule.imx214_3.bin \
	/usr/share/modalai/chi-cdk/imx214/com.qti.sensormodule.imx214_4.bin \
	/usr/share/modalai/chi-cdk/imx214-flip/com.qti.sensormodule.imx214_flip_0.bin \
	/usr/share/modalai/chi-cdk/imx214-flip/com.qti.sensormodule.imx214_flip_2.bin \
	/usr/share/modalai/chi-cdk/imx214-flip/com.qti.sensormodule.imx214_flip_3.bin \
	/usr/share/modalai/chi-cdk/imx214-flip/com.qti.sensormodule.imx214_flip_4.bin \
	"

# imx335
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx335/com.qti.sensormodule.imx335_0.bin \
	/usr/share/modalai/chi-cdk/imx335/com.qti.sensormodule.imx335_2.bin \
	/usr/share/modalai/chi-cdk/imx335/com.qti.sensormodule.imx335_3.bin \
	/usr/share/modalai/chi-cdk/imx335/com.qti.sensormodule.imx335_4.bin \
	"

# imx377
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx377/com.qti.sensormodule.imx377_3.bin \
	"

# imx412/577
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx412/com.qti.sensormodule.imx412_0.bin \
	/usr/share/modalai/chi-cdk/imx412/com.qti.sensormodule.imx412_2.bin \
	/usr/share/modalai/chi-cdk/imx412/com.qti.sensormodule.imx412_3.bin \
	/usr/share/modalai/chi-cdk/imx412/com.qti.sensormodule.imx412_4.bin \
	"

# imx412/577
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx412-flip/com.qti.sensormodule.imx412_flip_0.bin \
	/usr/share/modalai/chi-cdk/imx412-flip/com.qti.sensormodule.imx412_flip_2.bin \
	/usr/share/modalai/chi-cdk/imx412-flip/com.qti.sensormodule.imx412_flip_3.bin \
	/usr/share/modalai/chi-cdk/imx412-flip/com.qti.sensormodule.imx412_flip_4.bin \
	"

# imx412_fpv
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_0.bin \
	/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_1.bin \
	/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_2.bin \
	/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_3.bin \
	/usr/share/modalai/chi-cdk/imx412-fpv/com.qti.sensormodule.imx412_fpv_4.bin \
	"

# imx664
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_0.bin \
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_1.bin \
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_2.bin \
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_3.bin \
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_4.bin \
	/usr/share/modalai/chi-cdk/imx664/com.qti.sensormodule.imx664_5.bin \
	"

# imx678
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_0.bin \
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_mod_1.bin \
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_2.bin \
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_3.bin \
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_4.bin \
	/usr/share/modalai/chi-cdk/imx678/com.qti.sensormodule.imx678_mod_5.bin \
	"

# imx678_flip
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/imx678-flip/com.qti.sensormodule.imx678_flip_0.bin \
	/usr/share/modalai/chi-cdk/imx678-flip/com.qti.sensormodule.imx678_flip_2.bin \
	/usr/share/modalai/chi-cdk/imx678-flip/com.qti.sensormodule.imx678_flip_3.bin \
	/usr/share/modalai/chi-cdk/imx678-flip/com.qti.sensormodule.imx678_flip_4.bin \
	"

# boson
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_0.bin \
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_1.bin \
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_2.bin \
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_3.bin \
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_4.bin \
	/usr/share/modalai/chi-cdk/boson/com.qti.sensormodule.boson_5.bin \
	"

# tuning
FILES_${PN} += "\
	/usr/share/modalai/chi-cdk/tuned/com.qti.tuned.default.bin \
	/usr/share/modalai/chi-cdk/tuned/com.qti.tuned.imx214_m0024.bin \
	"
