FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# overrides file we use to customize camx/chi-cdk
SRC_URI =+ "file://camxoverridesettings.txt"

do_install_append() {
	# configure for use case (e.g. RB5 Flight 5 IFELite, 2 IFE)
	install -m 0666 ${WORKDIR}/camxoverridesettings.txt ${D}/vendor/etc/camera/
}
