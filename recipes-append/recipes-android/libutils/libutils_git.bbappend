FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI =+ "file://linux"

do_install_append() {
    # linux headers not in utils, but still adding to libutils-dev for easy installation in rootfs
    mkdir -p ${D}${includedir}/linux
    cp ${WORKDIR}/linux/* ${D}${includedir}/linux
}

FILES_${PN} += " \
    ${includedir}/linux/* \
"

do_package_qa[noexec] = "1"