# meta-voxl2

Open source layers added to the QRB5165 build

## recipes
A collection of general custom recipes used to modify the system image

## recipes-append
All recipes which append existing bitbake recipes. Notable bbappends are:
1. qti-ubuntu-robotics-image.bbapend : used to include packages from local recipes
2. ubuntu-base_18.04.bbapend : used to include packages that are from the apt repository. Equivalent to `apt-get`

## recipes-kernel
Kernel related recipes. Typically require kernel headers and generate kernel object files (.ko)
