inherit systemd

DESCRIPTION = "Enable SLPI restart"
SUMMARY     = "Enables SLPI to restart by setting restart level to relative on boot"
LICENSE     = "CLOSED"

SRC_URI = "file://en_slpi_restart.service"

EN_SLPI_RESTART = "en_slpi_restart.service"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "${EN_SLPI_RESTART}"

do_install() {
    # Install service file in sysfs
    install -d ${D}/${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/${EN_SLPI_RESTART} ${D}/${systemd_unitdir}/system
}
