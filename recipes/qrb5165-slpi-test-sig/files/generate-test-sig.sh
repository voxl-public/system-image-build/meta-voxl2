#!/bin/bash


NAME="testsig-util-0.2"
DIR="/share/modalai/qrb5165-slpi-test-sig"
TAR="${DIR}/${NAME}.tar.gz"
FLAG="${DIR}/is_installed.txt"
FORCE=false


print_usage(){
	echo ""
	echo " generates a new slpi test signature if it hasn't been made already"
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./generate_test_sig.sh"
	echo "        generate the sig if it's not already made"
	echo ""
	echo "  ./generate_test_sig.sh --help"
	echo "        show this help message"
	echo ""
	echo "  ./generate_test_sig.sh -f"
	echo "        generate anew one always"
	echo ""
	echo ""
}


process_argument () {

	## convert argument to lower case for robustness
	arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')
	case ${arg} in
		"")
			#echo "Making Normal Package"
			;;
		"-h"|"--help"|"help")
			print_usage
			exit 0
			;;
		"-f"|"--f"|"force"|"--force")
			FORCE=true
			;;
		*)
			echo "invalid option"
			print_usage
			exit 1
	esac
}


## parse all arguments or run wizard
for var in "$@"
do
	process_argument $var
done


if [ $FORCE == false ] && [ -f ${FLAG} ]; then
	echo "test signature is already installed, skipping"
	exit 0
fi


tar -xzmf ${TAR} -C /tmp

cd /tmp/${NAME}
./testsig-util.sh
cd ../

rm -rf /tmp/${NAME}
echo "true" > ${FLAG}

echo "DONE generating test signature"

exit 0


