DESCRIPTION = "QRB5165 slpi test signature"
SUMMARY     = "Temporary bitbake package that includes a test sig tool, in the future will replace to build package from within bb file"
LICENSE     = "CLOSED"

DEPENDS += " dpkg-native "

SRC_URI  = "file://generate-test-sig.sh"
SRC_URI += "file://testsig-util-0.2.tar.gz.zip"

do_install_append () {
    mkdir -p ${D}/share/modalai/qrb5165-slpi-test-sig/
    cp ${WORKDIR}/generate-test-sig.sh ${D}/share/modalai/qrb5165-slpi-test-sig/
    cp ${WORKDIR}/testsig-util-0.2.tar.gz ${D}/share/modalai/qrb5165-slpi-test-sig/
}

FILES_${PN} += " \
    /share/modalai/qrb5165-slpi-test-sig/* \
"

# Remove package QA to remove errors
do_package_qa[noexec] = "1"