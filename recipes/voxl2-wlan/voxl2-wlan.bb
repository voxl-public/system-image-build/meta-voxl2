DESCRIPTION = "voxl2-wlan"
SUMMARY     = "Packages for voxl2 that enable wifi dongle compatibility "
LICENSE     = "CLOSED"

SRC_URI  = "file://wpa_supplicant.conf"
SRC_URI += "file://wpa_supplicant.service"
SRC_URI += "file://hostapd.conf"

do_install() {
    # install hostapd conf file
    install -d ${D}/data/misc/wifi
    install -m 0666 ${WORKDIR}/hostapd.conf ${D}/data/misc/wifi

    # install wpa_supplicant.conf
    mkdir -p ${D}/data/misc/wifi/
    install -m 0664 ${WORKDIR}/wpa_supplicant.conf ${D}/data/misc/wifi/

    # install wpa_supplicant service file
    install -d ${D}/${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/wpa_supplicant.service ${D}/${systemd_unitdir}/system
}

FILES_${PN} += " \
    /data/misc/wifi/wpa_supplicant.conf \
    /data/misc/wifi/hostapd.conf \
    ${systemd_unitdir}/system/wpa_supplicant.service \
"
