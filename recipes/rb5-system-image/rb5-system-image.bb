inherit useradd

DESCRIPTION = "rb5 system image meta package"
SUMMARY     = "Meta package that is used to track system image version for DEPENDS in debian packages"
LICENSE     = "CLOSED"

# Package version should match system image version
PV = "${SYSTEM_IMAGE_VERSION}"

ALLOW_EMPTY_rb5-system-image = "1"

# used by software packages to determine platform
RPROVIDES_${PN} = "qrb5165-system-image system-image cci-direct-support"

# Add Groups to remove Group ID not found message when executing bash terminal
GROUPADD_PARAM_${PN} = "-g 1004 rcu_sched_1004; \
                        -g 1007 rcu_sched_1007; \
                        -g 1011 rcu_sched_1011; \
                        -g 1028 rcu_sched_1028; \
                        -g 3001 rcu_sched_3001; \
                        -g 3002 rcu_sched_3002; \
                        -g 3006 rcu_sched_3006; \
"

do_package_qa[noexec] = "1"
