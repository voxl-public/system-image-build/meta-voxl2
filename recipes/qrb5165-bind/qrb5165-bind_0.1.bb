DESCRIPTION = "QRB5165 bind"
SUMMARY     = "qrb5165 bind tool for binding to spektrum radios"
LICENSE     = "CLOSED"

SRC_URI = "file://qrb5165-bind"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/qrb5165-bind ${D}${bindir}
}

FILES_${PN} += "${bindir}/qrb5165-bind"