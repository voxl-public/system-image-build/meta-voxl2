DESCRIPTION = "Add machine vision library and 32-bit dependencies"
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI =  "file://lib"
SRC_URI =+ "file://lib32"
SRC_URI =+ "file://mv"
SRC_URI =+ "file://qcom-licenses"

# In current state mv package should always be empty, but package must be created in order for rootfs to build
ALLOW_EMPTY_mv = "1"

# IMAGE_INSTALL includes mv-voxl, avoid build errors if package is empty 
ALLOW_EMPTY_mv-voxl = "1"

do_install() {
    # Install Qualcomm license
    mkdir -p ${D}/opt/qcom-licenses
    cp ${WORKDIR}/qcom-licenses/* ${D}/opt/qcom-licenses

    # Install Qualcomm headers
    mkdir -p ${D}${includedir}/mv
    cp ${WORKDIR}/mv/* ${D}${includedir}/mv

    # Install loader
    mkdir -p ${D}${base_libdir_native}
    cp ${WORKDIR}/lib/ld-linux.so.3 ${D}${base_libdir_native}

    # Install 32-bit libraries
    mkdir -p ${D}${prefix_native}/lib32
    cp ${WORKDIR}/lib32/* ${D}${prefix_native}/lib32
}

# Other packages require mv/mv-dev, use custom voxl package
FILES_${PN}-voxl += " \
    /opt/qcom-licenses/* \
    ${includedir}/mv/* \
    ${prefix_native}/lib32/* \
    ${base_libdir_native}/ld-linux.so.3 \
"

# .so files and headers are placed into -dev, /lib/* are placed into PN,
# prepend mv-voxl in packages to have files go into mv-voxl package
PACKAGES =+ "${PN}-voxl"

do_package_qa[noexec] = "1"
