#ifndef MVDFS_H
#define MVDFS_H

/***************************************************************************//**
@file
   mvDFS.h

@detailed
   Machine Vision,
   Depth from Stereo (DFS)

@section Overview
   DFS finds the disparity pixels as the x-axis distance (in pixels) of one place 
   in the left image verses that same place in the right image.  The assumption
   of a stereo configuration of the cameras is leveraged for speed.  Therefore,
   this feature is not good for general feature matching.  The disparities are 
   mapped directly to the distance away from the camera.  A disparity value of 0
   would mean the object is at infinity whereas a disparity value of 28 would mean 
   that the object is very close.

   There are two algorithms supported for flexibility on whether to use the CPU
   or GPU.  However, the ALG1 on GPU is the primary and preferred algorithm.
   Although FPS speeds much greater are possible, a typical configuration 
   supporting 30 FPS for MVDFS_MODE_ALG1_GPU is:
   \code
   resolution = QVGA
   minDisparity = 0
   maxDisparity = 28  // detectable distance = 0.6m for focal length ~217 pel
   aggregationWindowSize = 11\endcode

@section Limitations
   The following list are some of the known limitations:
   - Cannot resolve depths > ~100*(distance between cameras).
   - Cannot resolve depth where the field of view does not overlap between
     both cameras.
   - Does not detect transparent, reflective, shiny smooth solid color, overly
     illuminated, or inadequately illuminated surfaces.
   - Does not detect some surfaces with repeating patterns.
   - Rig calibration must be good to < 0.5 pixels projection error between
     left and right images.
   - Does not detect linear object (e.g., power line) that run along same
     rows in images.

@internal
   Copyright 2017 Qualcomm Technologies, Inc.  All rights reserved.
   Confidential & Proprietary.
*******************************************************************************/


//==============================================================================
// Defines
//==============================================================================


//==============================================================================
// Includes
//==============================================================================

#include <mv.h>

//==============================================================================
// Declarations
//==============================================================================

#ifdef __cplusplus
extern "C"
{
#endif

   
   /************************************************************************//**
   @detailed
      Two different algorithms are currently supported on CPU and one on GPU.
      - MVDFS_MODE_ALG0_CPU:  Lower quality algorithm but very fast on CPU.
      - MVDFS_MODE_ALG1_CPU:  Higher quality algorithm but very slow on CPU.
        This mode is primarily for off-target testing since it is too slow for
        practical use.
      - MVDFS_MODE_ALG1_GPU:  Higher quality algorithm and very fast on GPU.
   ****************************************************************************/
   typedef enum
   {
      MVDFS_MODE_ALG0_CPU,
      MVDFS_MODE_ALG1_CPU,
      MVDFS_MODE_ALG1_GPU,
   } MVDFS_MODE;


   /************************************************************************//**
   @detailed
      Depth From Stereo (DFS).
   ****************************************************************************/
   typedef struct mvDFS mvDFS;


   /************************************************************************//**
   @detailed
      The parameters optionally use to initialize DFS.
   @param textureThreshold
      Filters out areas of the image without texture.  Range is [0 - 1] with 0
      meaning no threshold is applied. A good value is ~0.1.
   @param aggregationWindowWidth
      Size of the aggregation window.
   @param aggregationWindowHeight
      Size of the aggregation window.
   @param maxSpeckleSize
      If a block of pixels with similar disparity is smaller than maxSpeckleSize
      it will be removed.  Setting value to zero will disable this filtering.
   ****************************************************************************/
   struct mvDFSParameters
   {
      float32_t textureThreshold;
      uint32_t  aggregationWindowWidth;
      uint32_t  aggregationWindowHeight;
      uint32_t  maxSpeckleSize;

      MV_API mvDFSParameters();
   };


   /************************************************************************//**
   @detailed
      Initialize stereo object.
   @param pnConfig
      Pointer to configuration.
   @param mode
      Select which mode DFS algorithm should run in, i.e quality vs speed vs
      GPU vs GPU simulation.
   @param using10BitInput
      \n true:  Input images are 10 bits grayscale with 2 bytes / pixel.
      \n false:  Input images are 8 bits grayscale with 1 byte / pixel.
   @return
      Pointer to stereo object; returns NULL if failed.
   ****************************************************************************/
   MV_API mvDFS* mvDFS_Initialize( const mvStereoConfiguration* nConfig,
                                   MVDFS_MODE mode, bool using10bitInput, 
                                   const mvDFSParameters* params = NULL );


   /************************************************************************//**
   @detailed
      Deinitialize stereo object.
   @param pObj
      Pointer to stereo object.
   ****************************************************************************/
   void MV_API mvDFS_Deinitialize( mvDFS* pObj );


   /************************************************************************//**
   @detailed
      Compute inverse depth.
   @param pObj
      Pointer to stereo object.
   @param pxlsCamL
      Left camera image.
   @param pxlsCamR
      Right camera image.
   @param numMasks
      Number of rectangular masks.
   @param masks
      Mask defined as rectangular region in the depth image in which disparities
      and depth are to be masked out (set to 0).  A single region is defined by
      four integers being image coordinates of upper left and bottom right
      corners of the region; if NULL no masking is done.
   @param minDisparity
      Lower limit of the disparity range to be scanned.
      \n\b NOTE:  Should be multiple of 4 for optimal speeds.
   @param maxDisparity
      Upper limit of the disparity range to be scanned.
      \n\b NOTE:  Should be multiple of 4 for optimal speeds.
   @param disparities
      Optional disparity for each pixel in the left camera image.
      Caller allocates and provides buffer with dimensions of camera image.
   @param invDepth
      Optional depth for each pixel of left camera image in units 1/meters.
      Caller allocates the buffer of the size of camera image.  Returned 0
      values mean depth for given pixel is unknown.
   @remark
      Inverse depth is computed for pixels of rectified left image which is
      rotated w.r.t. the original left image by rectifying rotation
      To get rectifying rotations use function mvDFS_getRectifyingRotations.
   ****************************************************************************/
   void MV_API mvDFS_GetDepths( mvDFS* pObj, const uint8_t* pxlsCamL,
                                const uint8_t* pxlsCamR, uint16_t numMasks,
                                uint16_t* masks, int16_t minDisparity,
                                int16_t maxDisparity, uint16_t* disparities,
                                float32_t* invDepth );


   /************************************************************************//**
   @detailed
      Compute inverse depth from left and right images stored side-by-side in an
      ION memory buffer.
   @param pObj
      Pointer to stereo object.
   @param fileDesc
      ION memory file descriptor.
   @param hostPtr
      Host virtual address to ION memory buffer.  The GPU requires this to be
      aligned to the device page size.
   @param bufSize
      Size in bytes of the allocation ION buffer.
   @param numMasks
      Number of rectangular masks.
   @param masks
      Mask defined as rectangular region in the depth image in which disparities
      and depth are to be masked out (set to 0).  A single region is defined by
      four integers being image coordinates of upper left and bottom right
      corners of the region; if NULL no masking is done.
   @param minDisparity
      Lower limit of the disparity range to be scanned.
      \n\b NOTE:  Should be multiple of 4 for optimal speeds.
   @param maxDisparity
      Upper limit of the disparity range to be scanned.
      \n\b NOTE:  Should be multiple of 4 for optimal speeds.
   @param disparities
      Optional disparity for each pixel in the left camera image.
      Caller allocates and provides buffer with dimensions of camera image.
   @param invDepth
      Optional depth for each pixel of left camera image in units 1/meters.
      Caller allocates the buffer of the size of camera image.  Returned 0
      values mean depth for given pixel is unknown.
   @remark
      Inverse depth is computed for pixels of rectified left image which is
      rotated w.r.t. the original left image by rectifying rotation.  To get 
      rectifying rotations use function mvDFS_getRectifyingRotations.
   ****************************************************************************/
   void MV_API mvDFS_GetDepthsION( mvDFS* pObj, int fileDesc, void* hostPtr,
                                   size_t bufSize, uint16_t numMasks,
                                   uint16_t* masks, int16_t minDisparity,
                                   int16_t maxDisparity,
                                   uint16_t* disparities, float32_t* invDepth );


   /************************************************************************//**
   @detailed
      Rectification rotation matrices for left and right images as 3x3 rotation
      matrices.
   @param obj
      Pointer to stereo object.
   @param rot1
      Pointer to 3x3 matrix in which the rotation of left image returned.
   @param rot1
      Pointer to 3x3 matrix in which the rotation of left image returned.
   ****************************************************************************/
   void MV_API mvDFS_GetRectifyingRotations( mvDFS* obj, float32_t* rot1,
                                             float32_t* rot2 );


   /************************************************************************//**
   @detailed
      Depth camera.  This virtual depth camera is obtained during solving the 
      rectification problem.
   @param obj
      Pointer to stereo object.
   @param depthCamera
      Pointer to camera structure.
   ****************************************************************************/
   void MV_API mvDFS_GetDepthCameraConfiguration( mvDFS* obj,
                                           mvCameraConfiguration* depthCamera );


   /************************************************************************//**
   @detailed
      Returns rectified left and right gray scale image.
   @param obj
      Pointer to stereo object.
   @param rectL
      Pointer to rectified left image.
   @param rectR
      Pointer to rectified right image.
   ****************************************************************************/
   void MV_API mvDFS_GetRectifiedImages( mvDFS* obj, uint8_t* rectL,
                                         uint8_t* rectR );


   /************************************************************************//**
    @detailed
        Enables rectification adjustment and provides the required parameters.
    @param obj
        Pointer to stereo object.
    @param params
        Pointer to buffer containing the rectification adjustment parameters.
   @param numParams
       Number of rectification adjustment parameters.
   ****************************************************************************/
   void MV_API mvDFS_EnableRectAdjustment( mvDFS* obj, float* params,
                                           unsigned int numParams );


   /************************************************************************//**
   @detailed
       Disables rectification adjustment.
    @param obj
       Pointer to stereo object.
   ****************************************************************************/
   void MV_API mvDFS_DisableRectAdjustment( mvDFS* obj );



#ifdef __cplusplus
}
#endif


#endif
