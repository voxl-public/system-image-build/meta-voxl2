#ifndef MVDFT_H
#define MVDFT_H

/***************************************************************************//**
@file
   mvDFT.h

@detailed
   Machine Vision,
   Downward Facing Tracker (mvDFT)

@section Overview
   This feature provides frame-by-frame localization for cameras facing mostly
   straight down.

@section Limitations
   The following list are some of the known limitations:
   - Does not work over transparent, reflective, shiny smooth solid color, 
     overly illuminated, or inadequately illuminated surfaces.
   - Does not work over some surfaces with repeating patterns.
   - Camera calibration must be good to < 0.5 pixels re-projection error.
   - Velocity must be < 50 pixels/frame.

@internal
   Copyright 2017 Qualcomm Technologies, Inc.  All rights reserved.
   Confidential & Proprietary.
*******************************************************************************/


//==============================================================================
// Defines
//==============================================================================

//==============================================================================
// Includes
//==============================================================================

#include <mv.h>

//==============================================================================
// Declarations
//==============================================================================

#ifdef __cplusplus
extern "C"
{
#endif


   /************************************************************************//**
   @detailed
      Downward Facing Tracker (mvDFT).
   ****************************************************************************/
   typedef struct mvDFT mvDFT;


   /************************************************************************//**
   @detailed
      Configuration parameters for initializing mvDFT.
   ****************************************************************************/
   struct mvDFT_Configuration
   {
      mvCameraConfiguration camera;  /// camera intrinsic calibration params

      //This are parameters for optical flow grid
      int minNrFeatures;  /// minNrFeatures forced as input to optical flow 
                          /// (the fewer, the less stable in texture poor areas)
      int maxNrFeatures;  /// maxNrFeatures forced as input to optical flow 
                          /// (the more, the more stable, but also slower)
   };


   /************************************************************************//**
   @detailed
      2D displacement estimate from mvDFT + quality indicators.
   ****************************************************************************/
   struct mvDFT_Data
   {
      //pixels displacement in the image frame
      float32_t displacement[2];
      //relative scale movement
      float32_t deltaScale;
      //number of inliers after calculation of displacement
      int32_t sampleSize;
      //error metric, sum of squared error over sampleSize points
      float32_t errorSum;
      //frame-to-frame rotation around camera center point (yaw, pitch, roll)
      float32_t rotation[3];
   };


   /************************************************************************//**
   @detailed
      Initialize mvDFT object.
   @param pnConfig
      Pointer to configuration.
   @return
      Pointer to mvDFT object; returns NULL if failed.
   ****************************************************************************/
   MV_API mvDFT* mvDFT_Initialize( const mvDFT_Configuration* nConfig );


   /************************************************************************//**
   @detailed
      Deinitialize mvDFT object.
   @param pObj
      Pointer to mvDFT object.
   ****************************************************************************/
   void MV_API mvDFT_Deinitialize( mvDFT* pObj );


   /************************************************************************//**
   @detailed
      Pass camera frame to the mvDFT object.
   @param pObj
      Pointer to mvDFT object.
   @param time
      Timestamp of camera frame.
   @param pxls
      Pointer to camera frame data.
   ****************************************************************************/
   void MV_API mvDFT_AddImage( mvDFT* pObj, int64_t time, const uint8_t* pxls );


   /************************************************************************//**
   @detailed
      Displacement data.
   @param pObj
      Pointer to mvDFT object.
   @param data
      Pointer to mvDFT_Data data array.
   @return 
      Success or not.
   ****************************************************************************/
   bool MV_API mvDFT_GetResult( mvDFT* pObj, mvDFT_Data* data );


#ifdef __cplusplus
}
#endif


#endif
