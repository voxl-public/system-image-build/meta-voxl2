#ifndef MV_SAC_H
#define MV_SAC_H

/***************************************************************************//**
@file
   mvSAC.h

@detailed
   Machine Vision public API,
   Stereo Auto-Calibration (SAC)

@section Overview
   This module performs stereo camera auto-calibration, which does not require
   a known pattern in front of the camera. It calibrates the following
   parameters: rotation from left camera to right.

@section Limitations
   The following list are some of the known limitations:
   - Requires textured objects in front of the camera for tracking;
     Otherwise SAC will not return any result.
   - Exposure time must be shorter than 5ms; Otherwise SAC may return
     incorrect results.
   - Typically needs at least 3 seconds of data to produce good
     quality results.

@internal
   Copyright 2017 Qualcomm Technologies, Inc.  All rights reserved.
   Confidential & Proprietary.
*******************************************************************************/


//==============================================================================
// Defines
//==============================================================================

//==============================================================================
// Includes
//==============================================================================

#include <mv.h>

//==============================================================================
// Declarations
//==============================================================================

#ifdef __cplusplus
extern "C"
{
#endif


   /************************************************************************//**
   @detailed
      Stereo Auto-Calibration (SAC).
   ****************************************************************************/
   typedef struct mvSAC mvSAC;


   /************************************************************************//**
   @detailed
      Configuration parameters for initializing mvSAC.
   @param maxNumKeypoints
      Maximum number of key points used for tracking.
   ****************************************************************************/
   struct mvSACConfiguration
   {
      float32_t maxNumKeypoints = 1000;
   };


   /************************************************************************//**
   @detailed
      SAC status.
   @param reprojectionError
      Re-projection error in pixels.
   @param inlierRatio
      Inlier ratio.
   ****************************************************************************/
   struct mvSACStatus
   {
      float32_t reprojectionError;
      float32_t inlierRatio;
   };


   /************************************************************************//**
   @detailed
      Initialize Stereo Auto-Calibration (SAC) object.
   @param pCfgL
      Camera calibration parameters of the left camera.  Parameters 
      memoryStride and uvOffset are ignored.
   @param pCfgR
      Camera calibration parameters of the right camera.  Parameters
      memoryStride and uvOffset are ignored.
   @param translation
      Translation from left camera's coordinate to right camera's coordinate,
      measured in meters.
   @param pSACCfg
      SAC configuration parameters.
   @return
      Pointer to SAC object; returns NULL if failed.
   ****************************************************************************/
   MV_API mvSAC* mvSAC_Initialize( const mvCameraConfiguration* pCfgL,
                                   const mvCameraConfiguration* pCfgR,
                                   const float32_t translation[3],
                                   const mvSACConfiguration* pSACCfg );


   /************************************************************************//**
   @detailed
      Deinitialize Stereo Auto-Calibration (SAC) object.
   @param pObj
      Pointer to SAC object.
   ****************************************************************************/
   void MV_API mvSAC_Deinitialize( mvSAC* pObj );


   /************************************************************************//**
   @detailed
      Add camera frame.
   @param pObj
      Pointer to SAC object.
   @param pixelsL
      Pointer to the pixels of luma channel, for the left camera.
   @param strideL
      Stride of the luma channel in bytes, for the left camera.
   @param pixelsR
      Pointer to the pixels of luma channel, for the right camera.
   @param strideR
      Stride of the luma channel in bytes, for the right camera.
   ****************************************************************************/
   void MV_API mvSAC_AddFrame( mvSAC* pObj, const uint8_t* pixelsL,
                               uint32_t strideL, const uint8_t* pixelsR,
                               uint32_t strideR );


   /************************************************************************//**
   @detailed
      Get the calibration result.
   @param pObj
      Pointer to SAC object.
   @param pStereoCfg
      Stereo configuration.
   @param pStatus
      SAC status. Set to nullptr if not needed.
   @return
      Tracking state. No calibration result is returned if state < 0.
   ****************************************************************************/
   MV_TRACKING_STATE MV_API mvSAC_GetCalibration( mvSAC* pObj,
                                                  mvStereoConfiguration* pStereoCfg,
                                                  mvSACStatus* pStatus );

#ifdef __cplusplus
}
#endif

#endif
