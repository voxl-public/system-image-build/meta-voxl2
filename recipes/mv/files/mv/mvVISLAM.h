#ifndef MVVISLAM_H
#define MVVISLAM_H

/***************************************************************************//**
@file
   mvVISLAM.h

@detailed
   Machine Vision,
   Visual-Inertial Simultaneous Fusion Localization and Mapping (VISLAM)

@section Overview
   VISLAM provides 6-DOF localization and pose estimation for various 
   applications.  It has been tuned for robot use cases in particular.

   In addition to the initialization parameters, there are other things to 
   consider when attempting to get the best possible performance out of 
   VISLAM.  A good camera calibration performed specifically for a given camera 
   has the potential to significantly reduce the overall odometry drift rather 
   than the default calibration provided in examples.

   Furthermore, rich motion just after VISLAM starts can accelerate the state 
   space convergence and lead to lower drift.  For the drone application, 
   rolling/pitching and high linear accelerations are good types of motion for 
   better convergence.

   The spatial frame (s) used herein might also be called the world frame or 
   event the initialization frame since it is defined by where the feature 
   fully initializes and stabilizes.  The gravity-aligned spatial frame (s') might 
   also be called the inertial frame if the x-axis were aligned to North.  The 
   body frame (b) is centered on the accelerometer which may be slightly 
   different than the IMU or gyro.  The camera frame (c) is centered on the 
   camera.

@section Limitations
   The following list are some of the known limitations:
   - The state may drift before takeoff if the IMU cutoff frequency is set to
     below the frequency of vibration sources on the board (fan, propellers).
   - Landing in a scenario where the closest features are far away may not
     sufficiently constrain the position estimate, causing the drone to drift on
     the ground if GPS velocity estimates are not provided
   - Flying over water violates the assumption of feature stationary; system will
     reset if GPS velocity estimates are not provided
   - Flying at high altitudes drives up velocity uncertainty which
     can cause problems if all points (for which depth has converged) are lost
     after excessive yawing and GPS velocity estimates are not available.

@internal
   Copyright 2017 Qualcomm Technologies, Inc.  All rights reserved.
   Confidential & Proprietary.
*******************************************************************************/


//==============================================================================
// Defines
//==============================================================================


//==============================================================================
// Includes
//==============================================================================

#include <mv.h>

//==============================================================================
// Declarations
//==============================================================================

#ifdef __cplusplus
extern "C"
{
#endif

   /************************************************************************//**
   @detailed
      Pose information along with a quality indicator for VISLAM.
   @param poseQuality
      Quality of the pose (no pose is provided if MV_TRACKING_STATE_INITIALIZING
      or MV_TRACKING_STATE_FAILED). If the IMU measurement range or bandwidth is
      exceeded, MV_TRACKING_STATE_LOW_QUALITY is returned. In normal operation, 
      poseQuality should correspond to MV_TRACKING_STATE_HIGH_QUALITY.
   @param bodyPose
      Body pose estimate in rotation-translation matrix form 
      [ R_{sb} | T_{sb} ].  T_{sb} is the estimate of the translation of the 
      origin of the body (b) or accelerometer frame relative to the spatial (s)
      frame in the spatial frame (meters).  The spatial frame corresponds to the
      body frame at initialization.  R_{sb} is the estimate of the corresponding
      rotation matrix.
   @param gravityCameraPose
      Gravity aligned pose of camera estimate in rotation-translation matrix 
      form [ R_{cs'} | T_{cs'} ].  T_{cs'} is the estimate of the translation of
      the origin of the camera frame (c) relative to the gravity aligned spatial
      (s') frame in the camera frame (meters).  The gravity aligned spatial 
      frame corresponds to the body frame at initialization rotated to 
      compensate for pitch and roll.  R_{cs'} is the estimate of the 
      corresponding rotation matrix.
   @param errCovPose
      Error covariance matrix for bodyPose estimate.
   @param timeAlignment
      Camera IMU time misalignment estimate (seconds).
   @param velocity
      Velocity estimate, vsb, of origin of accelerometer (b=body) in spatial 
      frame (m/s).  The spatial frame corresponds to the body frame at 
      initialization.
   @param errCovVelocity
      Error covariance for velocity estimate vsb ((m/s)^2).
   @param angularVelocity
      Angular velocity estimate in body (accelerometer) frame (rad/s).
   @param gravity
      Gravity vector estimate in spatial frame (m/s^2).  The spatial frame 
      corresponds to the body frame at initialization.
   @param errCovGravity
      Error covariance for gravity estimate in spatial frame ((m/s^2)^2).
   @param wBias
      Gyro bias estimate (rad/s).
   @param aBias
      Accelerometer bias estimate (m/s^2).
   @param Rbg
      Accelerometer gyro rotation matrix estimate 
      (b = body = accelerometer, g = gyro).
   @param aAccInv
      Inverse of accelerometer scale and non-orthogonality estimate.
   @param aGyrInv
      Inverse of gyro scale and non-orthogonality estimate.
   @param tbc
      Accelerometer-camera translation misalignment vector estimate (meters).
      t_{bc} is the estimate of the translation of the origin of the camera (c)
      frame relative to that of the body (b) or accelerometer frame in the body
      frame. 
   @param Rbc
      Accelerometer-camera rotational misalignment matrix estimate.  Can be used
      together with tbc to rotate vector in camera frame x_c to IMU frame x_imu 
      via [Rbc|tbc]x_c = x_imu. 
   @param errorCode
      Error code (includes reasons for reset) bit:
      - \b 0:  Reset: cov not pos definite
      - \b 1:  Reset: IMU exceeded range
      - \b 2:  Reset: IMU bandwidth too low
      - \b 3:  Reset: not stationary at initialization
      - \b 4:  Reset: no features for x seconds
      - \b 5:  Reset: insufficient constraints from features
      - \b 6:  Reset: failed to add new features
      - \b 7:  Reset: exceeded instant velocity uncertainty
      - \b 8:  Reset: exceeded velocity uncertainty over window
      - \b 10:  Dropped IMU samples
      - \b 11:  Intrinsic camera cal questionable
      - \b 12:  Insufficient number of good features to initialize
      - \b 13:  Dropped camera frame
      - \b 14:  Dropped GPS velocity sample
      - \b 15:  Sensor measurements with uninitialized time stamps or uninitialized uncertainty (set to 0)
      If a reset occurs, the last "good" pose will be used after initialization. To reset the pose, call 
      mvVISLAM_Reset( mvVISLAM* pObj, bool resetPose ) with resetPose set to true.
   @param time
      Timestamp of pose in nanoseconds in system time. 
   ****************************************************************************/
   struct mvVISLAMPose
   {
      MV_TRACKING_STATE poseQuality;
      mvPose6DRT bodyPose;
      mvPose6DRT gravityCameraPose;
      float32_t errCovPose[6][6];
      float32_t timeAlignment;
      float32_t velocity[3];
      float32_t errCovVelocity[3][3];
      float32_t angularVelocity[3];
      float32_t gravity[3];
      float32_t errCovGravity[3][3];
      float32_t wBias[3];
      float32_t aBias[3];
      float32_t Rbg[3][3];
      float32_t aAccInv[3][3];
      float32_t aGyrInv[3][3];
      float32_t tbc[3];
      float32_t Rbc[3][3];
      uint32_t errorCode;
      int64_t time;
   };


   /************************************************************************//**
   @detailed
      Map point information from VISLAM.
   @param id
      Unique ID for map point.
   @param pixLoc
      2D measured pixel location in pixels.
   @param tsf
      3D location in spatial frame in meters.
   @param p_tsf
      Error covariance for tsf.
   @param depth
      Depth of map point from camera in meters.
   @param depthErrorStdDev
      Depth error standard deviation in meters.
   @param pointQuality
      Quality of the map point as per current VISLAM state.
   ****************************************************************************/
   struct mvVISLAMMapPoint
   {
      enum QUALITY_T
      {
         LOW,     ///< additional low-quality points collected for e.g. collision avoidance
         MEDIUM,  ///< Points that are not "in state"
         HIGH     ///< Points that are "in state"
      };
      uint32_t  id;
      float32_t pixLoc[2];
      float32_t tsf[3];
      float32_t p_tsf[3][3];
      float32_t depth;
      float32_t depthErrorStdDev;
      QUALITY_T pointQuality;
   };



   /************************************************************************//**
   @detailed
      Visual-Inertial Simultaneous Fusion Localization And Mapping (VISLAM)
   ****************************************************************************/
   typedef class mvVISLAM mvVISLAM;


   /************************************************************************//**
   @detailed
      Initialize VISLAM object.  A few parameters may significantly impact the 
      performance of the VISLAM algorithm.  Some parameters affect the initial 
      convergence of VISLAM, which impacts the overall drift in the estimated 
      pose.  The following parameters should have particular attention paid to
      them:  logDepthBootstrap, useLogCameraHeight, logCameraHeightBootstrap, 
      and limitedIMUbWtrigger.
   @param camera
      Pointer to camera intrinsic calibration parameters.
   @param readoutTime
      Frame readout time (seconds). n times row readout time. Set to 0 for 
      global shutter camera.  Frame readout time should be (close to) but 
      smaller than the rolling shutter camera frame period.
   @param tbc
      Pointer to accelerometer-camera translation misalignment vector (meters). 
      T_{bc} is the translation of the origin of the camera (c) frame relative 
      to that of the body (b) or accelerometer frame in the body frame.  T_{bc}
      setting can be verified by checking estimated T_{bc}.
   @param ombc 
      Pointer to accelerometer-camera misalignment vector (radians). \Omega_{bc}
      is the corresponding rotation in exponential coordinates. Can be used 
      together with T_{bc} to rotate vector in camera frame x_c to IMU frame 
      x_imu via [R|T]x_c = x_imu.  \Omega_{bc} settings can be verified by 
      checking estimated R_{bc} mapped to exponential coordinates.
   @param delta
      Camera-inertial timestamp misalignment (seconds). Ideally this is within 
      about 1 ms of the true value.  Delta can be verified by checking the 
      estimated time alignment.
   @param std0Tbc
      Pointer to initial uncertainty in accelerometer-camera translation vector
      (meters).
   @param std0Ombc
      Pointer to initial uncertainty in accelerometer-camera orientation vector
      (rad.).
   @param std0Delta
      Initial uncertainty in time misalignment estimate (seconds).
   @param accelMeasRange
      Accelerometer sensor measurement range (m/s^2).
   @param gyroMeasRange
      Gyro sensor measurement range (rad./s).
   @param stdAccelMeasNoise
      Standard deviation of accelerometer measurement noise (m/s^2).
   @param stdGyroMeasNoise
      Standard deviation of gyro measurement noise (rad./s).
   @param stdCamNoise
      Standard dev of camera noise per pixel.
   @param minStdPixelNoise
      Minimum of standard deviation of feature measurement noise in pixels.
   @param failHighPixelNoiseScaleFactor
      Scales measurement noise and compares against search area (is search area
      large enough to reliably compute measurement noise covariance matrix).
   @param logDepthBootstrap
      Initial point depth [log(meters)], where log is the natural log. By 
      default, initial depth is set to 1m.  However, if e.g. a downward facing 
      camera on a drone is used and it can be assumed that feature depth at 
      initialization is always e.g. 4cm, then we can set this parameter to 4cm 
      (or -3.2). This will improve tracking during takeoff, accelerate state 
      space convergence, and lead to more accurate and robust pose estimates.
   @param useLogCameraHeight
      Use logCameraHeightBootstrap instead of logDepthBootstrap.
   @param logCameraHeightBootstrap
      Initializes point depth based on known geometry, assumes (1) camera 
      pointing partially at ground plane and (2) board/IMU aligned with gravity 
      at start (= accelerometer measures roughly [0, 0, -9.8] in units of 
      m/s^2), required input is camera height over ground (log(meters)), log is 
      natural log.  Understanding when to use logDepthBootstrap versus 
      logCameraHeightBootstrap and how to set these values appropriately can 
      improve the initialization of VISLAM and has the potential to reduce the 
      amount of odometry drift observed.
   @param noInitWhenMoving
      Set if device is stationary w.r.t. surface when initializing (e.g. drone) 
      based on camera, not on IMU: supports device on moving surface.
   @param limitedIMUbWtrigger
      To prevent tracking failure during/right after (hard) landing: 
      If sum of 3 consecutive accelerometer samples in any dimension divided by 4.3 
      exceed this threshold, IMU measurement noise is increased (and resets 
      become more likely); 
      \n\b NOTE: if platform vibrates heavily during flight, this may trigger mid-
      flight; if poseQuality in mvVISLAMPose drops to MV_TRACKING_STATE_LOW_QUALITY 
      during flight, improve mechanical dampening (and/or increase threshold) 
      \n RECOMMEND:  150m/s^2 / 4.3 ~= 35 
   @param staticMaskFileName
      1/4 resolution image (w.r.t. VGA), 160x120, PGM format, 
      the part of the camera view for which pixels are set to 255 is blocked 
      from feature detection useful, e.g., to avoid detecting & tracking points
      on landing gear reaching into camera view.
   @param gpsImuTimeAlignment 
      GPS-inertial timestamp misalignment (seconds), negative if GPS time 
      stamping is delayed relative to IMU time stamping, ideally this is within
      about 1 ms of the true value.
   @param tba
      Pointer to accelerometer-GPS antenna translation misalignment vector/lever
      arm (meters).  T_{ba} is the translation of the origin of the GPS antenna 
      (a) frame relative to that of the body (b) or accelerometer frame in the 
      body frame.
   @param mapping
      Flag to enable simple mapping: Feature points that leave the camera view are 
      stored. If they move back into camera view and they are within the search
      area of front-end tracker, they will be re-used.
   @return
      Pointer to VISLAM object; returns NULL if failed.
   ****************************************************************************/
   MV_API mvVISLAM* mvVISLAM_Initialize(
      const mvCameraConfiguration* camera, float32_t readoutTime,
      const float32_t* tbc, const float32_t* ombc, float32_t delta,
      const float32_t* std0Tbc, const float32_t* std0Ombc, 
      float32_t std0Delta, float32_t accelMeasRange, 
      float32_t gyroMeasRange, float32_t stdAccelMeasNoise, 
      float32_t stdGyroMeasNoise, float32_t stdCamNoise, 
      float32_t minStdPixelNoise,  float32_t failHighPixelNoiseScaleFactor,
      float32_t logDepthBootstrap, bool useLogCameraHeight, 
      float32_t logCameraHeightBootstrap, bool noInitWhenMoving, 
      float32_t limitedIMUbWtrigger, const char* staticMaskFileName,
      float32_t gpsImuTimeAlignment, const float32_t* tba,
      bool mapping );


   /************************************************************************//**
   @detailed
      Deinitialize VISLAM object.
   @param pObj
      Pointer to VISLAM object.
   ****************************************************************************/
   void MV_API mvVISLAM_Deinitialize( mvVISLAM* pObj );


   /************************************************************************//**
   @detailed
      Add the camera frame to the VISLAM object and trigger processing (a frame 
      update) on the newly added image while utilizing any already added IMU 
      samples, including timestamps occurring after the image, to fully 
      propagate the pose forward to the most recent IMU sample.
      \n\b NOTE:  All other sensor data occurring before this image must be
      added first before calling this function otherwise that older data will
      be dropped at the next call of this function.
   @param pObj
      Pointer to VISLAM object.
   @param time
      Timestamp of camera frame in nanoseconds in system time.  Time must be 
      center of exposure time, not start of frame or end of frame.
   @param pxls
      Pointer to camera frame 8-bit grayscale luminance data (VGA).
   ****************************************************************************/
   void MV_API mvVISLAM_AddImage( mvVISLAM* pObj, int64_t time, 
                                  const uint8_t* pxls );


   /************************************************************************//**
   @detailed
      Pass Accelerometer data to the VISLAM object.
   @param pObj
      Pointer to VISLAM object.
   @param time
      Timestamp of data in nanoseconds in system time.
   @param x
      Accelerometer data for X axis in m/s^2.
   @param y
      Accelerometer data for Y axis in m/s^2.
   @param z
      Accelerometer data for Z axis in m/s^2.
   ****************************************************************************/
   void MV_API mvVISLAM_AddAccel( mvVISLAM* pObj, int64_t time,
                                  float64_t x, float64_t y, float64_t z );


   /************************************************************************//**
   @detailed
      Pass Gyroscope data to the VISLAM object.
   @param pObj
      Pointer to VISLAM object.
   @param time
      Timestamp of data in nanoseconds in system time.
   @param x
      Gyro data for X axis in rad/s.
   @param y
      Gyro data for Y axis in rad/s.
   @param z
      Gyro data for Z axis in rad/s.
   ****************************************************************************/
   void MV_API mvVISLAM_AddGyro( mvVISLAM* pObj, int64_t time,
                                 float64_t x, float64_t y, float64_t z );

   
   /************************************************************************//**
   @detailed
      Pass GPS velocity data to the VISLAM object 
   @param pObj
      Pointer to VISLAM object
   @param time
      Timestamp of data in GPS time in nanoseconds 
   @param velocityEast
      GPS velocity data in East direction in m/s.
   @param velocityNorth
      GPS velocity data in North direction in m/s.
   @param velocityUP
      GPS velocity data in Up direction in m/s.
   @param measCovVelocity
      GPS velocity measurement error co-variance, fields in (m/s)^2.
   @param solutionInfo
      Fix type/quality: the last 3 bits being '100' represents a good message
      (if available, otherwise set to 4).
   ****************************************************************************/
   void MV_API mvVISLAM_AddGPSvelocity(mvVISLAM* pObj, int64_t time, 
       float64_t velocityEast, float64_t velocityNorth, float64_t velocityUP, 
       float64_t measCovVelocity[3][3], uint16_t solutionInfo);


   /************************************************************************//**
   @detailed
      Pass GPS time bias data to the VISLAM object.
   @param pObj
      Pointer to VISLAM object.
   @param time
      Timestamp of data in system time in nanoseconds.
   @param bias
      Time bias/offset (time bias/offset = GPS time - system time) in 
      nanoseconds.
   @param gpsTimeStdDev
      GPS time uncertainty (if available, otherwise set to -1).
   ****************************************************************************/
   void MV_API mvVISLAM_AddGPStimeSync( mvVISLAM* pObj, int64_t time, 
                                        int64_t bias, int64_t gpsTimeStdDev );


   /************************************************************************//**
   @detailed
      Grab last computed pose.
   @param pObj
      Pointer to VISLAM object.
   @return
      Computed pose from previous frame and IMU data.
   ****************************************************************************/
   mvVISLAMPose MV_API mvVISLAM_GetPose( mvVISLAM* pObj );


   /************************************************************************//**
   @detailed
      Inquire if VISLAM has new map points.
   @param pObj
      Pointer to VISLAM object.
   @return
      Number of map points currently being observed and estimated.
   ****************************************************************************/
   int MV_API mvVISLAM_HasUpdatedPointCloud( mvVISLAM *pObj );


   /************************************************************************//**
   @detailed
      Grab point cloud.
   @param pObj
      Pointer to VISLAM object.
   @param pPoints
      Pre-allocated array of mvVISLAMMapPoint structure to be filled in by
      VISLAM with current map points.
   @param maxPoints
      Max number of points requested.  Should match allocated size of pPoints.
   @return
      Number of points filled into the pPoints array.
   ****************************************************************************/
   int MV_API mvVISLAM_GetPointCloud( mvVISLAM* pObj, mvVISLAMMapPoint* pPoints,
                                      uint32_t maxPoints );


   /************************************************************************//**
   @detailed
      Resets the EKF from an external source. EKF will try to reinitialize in 
      the subsequent camera frame.  To properly initialize after reset, device 
      should not be rotating, moving a lot, camera look at 10+ features.  
   @param pObj
      Pointer to VISLAM object.
   @param resetPose
       false: initializes with last good pose after triggering reset
       true: initializes with "zero" pose after triggering reset
   ****************************************************************************/
   void MV_API mvVISLAM_Reset( mvVISLAM* pObj, bool resetPose );


#ifdef __cplusplus
}
#endif


#endif
