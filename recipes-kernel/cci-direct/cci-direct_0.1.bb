DESCRIPTION = "CCI Direct"
SUMMARY     = "Kernel headers required for CCI-direct"
LICENSE     = "CLOSED"

# Define basic paths
KERNEL_PATH="${TOPDIR}/../src/kernel/msm-qrb5165-4.19"
KERNEL_TECHPACK_MEDIA = "${KERNEL_PATH}/techpack/camera/include/uapi/media"
KERNEL_TECHPACK_SENSOR_UTIL = "${KERNEL_PATH}/techpack/camera/drivers/cam_sensor_module/cam_sensor_utils"

PACKAGES = "${PN}"

do_install() {
    mkdir -p ${D}${includedir}/media
    cp ${KERNEL_TECHPACK_MEDIA}/cam_defs.h     ${D}${includedir}/media
    cp ${KERNEL_TECHPACK_MEDIA}/cam_sensor.h   ${D}${includedir}/media
    cp ${KERNEL_TECHPACK_MEDIA}/cam_req_mgr.h  ${D}${includedir}/media

    mkdir -p ${D}${includedir}/cam_sensor_utils
    cp ${KERNEL_TECHPACK_SENSOR_UTIL}/cam_sensor_cmn_header.h ${D}${includedir}/cam_sensor_utils
}

FILES_${PN} += " \
   ${includedir}/media \
   ${includedir}/cam_sensor_utils \
"