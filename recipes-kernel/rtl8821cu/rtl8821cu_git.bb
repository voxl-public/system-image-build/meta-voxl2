inherit module

DESCRIPTION = "rtl8821cu"
SUMMARY     = "rtl8821cu kernel driver for wifi dongle support"
LICENSE     = "CLOSED"

# using static revision in order to avoid build failures with branch updates
SRCREV = "963666429f4f0ba63f3910b1d467cf184ba59892"
SRC_URI  = "git://github.com/modalai/8821cu-20210916;protocol=https;branch=v5.12.0.4"

S = "${WORKDIR}/git"

# rtl8188eus release
PV = "5.12.0.4-git"

DEPENDS = "virtual/kernel"
MODULES_INSTALL_TARGET="install"

do_install () {
    # move kernel module into rootfs
    install -d ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
    install -m 0644 ${B}/8821cu.ko ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/8821cu.ko
}

# Compile kernel module for rtl8188eus wifi driver
do_compile () {
    bbnote make -j 32 "$@"
    make -j 32 KSRC=${STAGING_KERNEL_BUILDDIR} ARCH=${ARCH}
}

FILES_${PN} += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/8821cu.ko "