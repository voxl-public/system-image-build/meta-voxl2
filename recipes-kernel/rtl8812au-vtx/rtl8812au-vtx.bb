inherit linux-kernel-base module

DESCRIPTION = "rtl8812au_vtx driver"
SUMMARY     = "WiFi Broadcast port of rtl8812vtx"
LICENSE     = "CLOSED"

# using static revision in order to avoid build failures with branch updates
SRCREV = "cf7cae7916dc7421b0f1e1859a302cfadc1815d5"
SRC_URI  = "git://gitlab.com/voxl-public/system-image-build/rtl8812au-vtx;protocol=https;branch=dev "

S = "${WORKDIR}/git"

KERNEL_MODULE_PACKAGE_NAME = "rtl8812au-vtx"

# rtl8812au release
#PV = "5.2.20-git"

DEPENDS = "virtual/kernel"
MODULES_INSTALL_TARGET="install"

# Compile kernel module for RTL8812AU wifi driver
do_compile () {
    bbnote make -j 32 "$@"
    KBUILD_NOPEDANTIC=1 make -j 32 KSRC=${STAGING_KERNEL_BUILDDIR}
}

do_install () {
    # move kernel module into rootfs
    install -d ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
    install -m 0644 ${B}/88XXau_vtx.ko ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/88XXau_vtx.ko
}

# Include files in main package
FILES_${PN} += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/88XXau_vtx.ko"
