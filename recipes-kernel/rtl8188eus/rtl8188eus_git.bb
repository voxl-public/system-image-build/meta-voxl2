inherit module

DESCRIPTION = "rtl8188eus"
SUMMARY     = "rtl8188eus kernel driver for wifi dongle support"
LICENSE     = "CLOSED"

# using static revision in order to avoid build failures with branch updates
SRCREV = "6146193406b62e942d13d4d43580ed94ac70c218"
SRC_URI  = "git://github.com/aircrack-ng/rtl8188eus;protocol=https;branch=v5.3.9 "

S = "${WORKDIR}/git"

# rtl8188eus release
PV = "5.3.9-git"

DEPENDS = "virtual/kernel"

MODULES_INSTALL_TARGET="install"

do_install () {
    # move kernel module into rootfs
    install -d ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
    install -m 0644 ${B}/8188eu.ko ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/8188eu.ko
}

# Compile kernel module for rtl8188eus wifi driver
do_compile () {
    bbnote make -j 32 "$@"
    make -j 32 KSRC=${STAGING_KERNEL_BUILDDIR} ARCH=${ARCH}
}

FILES_${PN} += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/8188eu.ko "