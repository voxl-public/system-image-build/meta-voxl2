inherit module

DESCRIPTION = "rtl8812au driver"
SUMMARY     = "RTL8812au kernel driver (wifi) and supplemental files for enabling wifi on voxl2"
LICENSE     = "CLOSED"

# using static revision in order to avoid build failures with branch updates
SRCREV = "3a6402e9e79802891f1531b435be54f4d8b71f0b"
SRC_URI  = "git://github.com/aircrack-ng/rtl8812au;protocol=https;branch=v5.6.4.2 "

S = "${WORKDIR}/git"

# rtl8812au release
PV = "5.6.4.2-git"

DEPENDS = "virtual/kernel"
MODULES_INSTALL_TARGET="install"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# SRC_URI += "file://ENABLE_CONCURRENT_MODE.patch;patchdir=${S}"

do_install () {
    # move kernel module into rootfs
    install -d ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
    install -m 0644 ${B}/88XXau.ko ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/88XXau.ko
}

# Compile kernel module for RTL8812AU wifi driver
do_compile () {
    bbnote make -j 32  "$@"
    make -j 32 KSRC=${STAGING_KERNEL_BUILDDIR} ARCH=${ARCH}
}

# Include files in main package
FILES_${PN} += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless/88XXau.ko"
